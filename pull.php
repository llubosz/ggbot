<?
require_once('api/MessageBuilder.php');

$M = new MessageBuilder();

function Parse($from, $data) 
{
    if (preg_match('/(radd|rdel|pass)\ +(.*)/', $data, $matches))
    {
	switch ($matches[1])
	{
	    case "radd": return ReminderAdd($from, $matches[2]); break;
	    case "rdel": return ReminderDel($from, $matches[2]); break;
	    case "pass": return PasswordGenerator(intval($matches[2])); break;
	}
    }
    else
    {
	return Clipboard($data, $from);	
    }
}

function PasswordGenerator($length)
{
    $length = ($length?$length:14);
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass);
} 

function ReminderAdd($from, $data)
{
        $mongo = new MongoClient();
        $db = $mongo->reminder;
        $collection = $db->$from;
        $entry = array("text" => $data, "time" => microtime());
        $collection->insert($entry);
        return "ok";
}

function ReminderDel($from, $data)
{
	$mongo = new MongoClient();
        $db = $mongo->reminder;
        $collection = $db->$from;
        $collection->remove(array("_id" => new MongoId($data)));
        return "ok";
}

function Reminder($from)
{
        $mongo = new MongoClient();
        $db = $mongo->reminder;
        $collection = $db->$from;
        $cursor = $collection->find();

        $output = "";
        foreach ($cursor as $no=>$entry)
        {
            //$output .= $entry['_id']."[br]";
            $output .= $no." [b]".$entry['text']."[/b][br]";
        }
        return $output;
}

function WotStats() 
{
    $profile = file_get_contents('http://worldoftanks.eu/community/accounts/503946882-KKefir/');
    preg_match('/minor"> Victories\<.*?value.*?\ (.*?)\ /s', $profile, $matches);
    $ile = $matches[1];
    return 'Cześć :) Kefir wygrał '.$ile.' bitew';
}

function Clipboard($data, $from)
{
	$mongo = new MongoClient();
	$db = $mongo->clipboard;
	$collection = $db->$from;
	$entry = array("text" => $data, "time" => microtime());
	$collection->insert($entry);
	return "zapisałem";
}

function GetAll($from)
{
	$mongo = new MongoClient();
        $db = $mongo->clipboard;
        $collection = $db->$from;
	$cursor = $collection->find();
	
	$output = "";
	foreach ($cursor as $entry)
	{
	    //$output .= $entry['_id']."[br]";
	    $output .= $entry['text']."[br][br]";
	}
	return $output;
}

function Kill($from, $id)
{
}

function Clear($from)
{
	$mongo = new MongoClient();
        $db = $mongo->clipboard;
        $collection = $db->$from;
        $collection->remove();

        return "schowek wyczyszczony";
}

switch ($HTTP_RAW_POST_DATA) {
    case "clear": $M->addText(Clear($_GET['from'])); break;
    case "show": $M->addBBcode(GetAll($_GET['from'])); break;
    case "ile wygrał?": $M->addText(WotStats()); break;
    case "help": $M->addBBcode("rem - show reminders[br]radd - add reminder[br]rdel - del reminder[br]pass n - generate n char password"); break;
    case "debug": $M->addText(var_dump($_GET)."\n".var_dump($HTTP_RAW_POST_DATA)); break;
    case "rem": $M->addBBcode(Reminder($_GET['from'])); break;
    case "lob": $M->addText("lob lob lob"); break;
    default: $M->addText(Parse($_GET['from'], $HTTP_RAW_POST_DATA));
}
$M->reply();
